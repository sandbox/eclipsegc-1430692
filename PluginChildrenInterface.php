<?php
/**
 * @file
 * Interface implemented by plugins needing children support.
 */

namespace Drupal\Plugin;

/**
 * Plugin interface for child plugin handling.
 */
interface PluginChildrenInterface extends PluginInterface {

  /**
   * Set a specific child for the instance to return.
   *
   * @param $child
   *   An identifier for the plugin implementation to determing which child to
   *   configuration to return.
   * @return
   *   A Configuration object specific to the child.
   */
  public function getChild($child);

  /**
   * Get the configurations for all possible children of this plugin instance.
   *
   * @return
   *   An array of Configuration objects for all potential children of this
   *   plugin implementation.
   */
  public function getChildren();

}
