<?php
/**
 * @file
 * Default plugin mapper object for general plugin use cases.
 */

namespace Drupal\Plugin;

use Drupal\Plugin\Factory\FactoryInterface;

/**
 * Default Drupal mapper object.
 *
 * It handles mapping of plugin requests to their factory and returns plugin
 * instances.
 */
class Mapper implements MapperInterface {
  protected $factories = array();
  protected $configuration = array();

  /**
   * Implements MapperInterface::getPluginType().
   */
  function getPluginType($scope, $type) {
    if (empty($this->configuration[$scope][$type])) {
      $this->configuration[$scope][$type] = config("plugin.type.$scope.$type");
    }
    return $this->configuration[$scope][$type];
  }

  /**
   * Implements MapperInterface::getFactoryClass().
   */
  public function getFactoryClass($scope, $type) {
    $plugin_type = $this->getPluginType($scope, $type);
    $factory = $plugin_type->get('factory');
    if (empty($factory)) {
      throw new MapperException("Factory class not found in configuration.");
    }
    return $factory;
  }

  /**
   * Implements MapperInterface::getFactory().
   */
  public function getFactory($scope, $type) {
    if (!isset($this->factories[$scope][$type])) {
      $factory_class = $this->getFactoryClass($scope, $type);
      if (!class_exists($factory_class)) {
        throw new MapperException("New factory class was not successfully created.");
      }

      $this->factories[$scope][$type] = new $factory_class($scope, $type, $this->configuration[$scope][$type]);
      if (!$this->factories[$scope][$type] instanceof FactoryInterface) {
        throw new MapperException("Unable to load a valid factory.");
      }
    }
    return $this->factories[$scope][$type];
  }

  /**
   * Implements MapperInterface::getPlugins().
   */
  public function getPlugins($scope, $type, $plugin = NULL) {
    $plugin_type = $this->getPluginType($scope, $type);
    $definition = $plugin_type->get('definition');
    if (empty($definition)) {
      $definition = "plugin.definition";
    }
    if (!empty($plugin)) {
      return config("$definition.$scope.$type.$plugin");
    }
    return config_get_signed_file_storage_names_with_prefix("$definition.$scope.$type");
  }

  /**
   * Implements MapperInterface::getPluginInstance().
   */
  public function getPluginInstance($scope, $type, $options = array()) {
    $factory = $this->getFactory($scope, $type);

    return $factory->getInstance($options);
  }

}
