<?php
/**
 * @file
 * Interface implemented by all plugins.
 */

namespace Drupal\Plugin;
use Drupal\Config\DrupalConfig;

/**
 * Plugin factory interface for child plugin handling.
 */
interface PluginInterface {

  /**
   * Construct a plugin instance.
   *
   * Passing in the configuration and context objects for the plugin to utilize
   * internally.
   *
   * @param Configuration $config
   *   A configuration object containing information about the plugin instance.
   */
  public function __construct(DrupalConfig $config = NULL);

}
