<?php
/**
 * @file
 * Base exception object for grouping mapper exceptions.
 */

namespace Drupal\Plugin;

use Exception;

/**
 * Exception class used for mapper related errors.
 */
class MapperException extends Exception {}
