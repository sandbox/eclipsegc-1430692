<?php
/**
 * @file
 * Mapper interface documenting the methods provided by plugin mapper objects.
 *
 * Mapper objects incorporate the best practices of retrieving configurations,
 * type information, and factory instantiation.
 */

namespace Drupal\Plugin;

/**
 * Interface definition for the mapper object.
 */
interface MapperInterface {

  /**
   * Get a plugin type object
   *
   * @param string $scope
   *   Provides a scoped namespace for the plugin type being requested. This
   *   allows for providers to create identically named plugin types without
   *   needing to worry about panels and views caching plugins being delivered
   *   for core cache plugin requests.
   *
   * @param string $type
   *   The plugin type for the requested scope.
   *
   * @return object
   *   The plugin type object.
   */
  public function getPluginType($scope, $type);

  /**
   * Gets a string representation of the factory class for use in the requested
   * plugin type.
   *
   * @param string $scope
   *   Provides a scoped namespace for the plugin type being requested. This
   *   allows for providers to create identically named plugin types without
   *   needing to worry about panels and views caching plugins being delivered
   *   for core cache plugin requests.
   *
   * @param string $type
   *   The plugin type for the requested scope.
   *
   * @return string
   *   The factory class name.
   */
  public function getFactoryClass($scope, $type);

  /**
   * Gets a string representation of the factory class for use in the requested
   * plugin type.
   *
   * @param string $scope
   *   Provides a scoped namespace for the plugin type being requested. This
   *   allows for providers to create identically named plugin types without
   *   needing to worry about panels and views caching plugins being delivered
   *   for core cache plugin requests.
   *
   * @param string $type
   *   The plugin type for the requested scope.
   *
   * @return string
   *   The instantiated factory class.
   */
  public function getFactory($scope, $type);

  /**
   * Get all plugin definitions for a particular scope/type combination or get
   * a specific plugin definition.
   *
   * @param string $scope
   *   Provides a scoped namespace for the plugin type being requested. This
   *   allows for providers to create identically named plugin types without
   *   needing to worry about panels and views caching plugins being delivered
   *   for core cache plugin requests.
   *
   * @param string $type
   *   The plugin type for the requested scope.
   *
   * @param string $plugin
   *   An optional plugin id.
   *
   * @return mixed
   *   An array of configuration names or a plugin definition configuration
   *   object.
   */
  public function getPlugins($scope, $type, $plugin = NULL);

  /**
   * Instantiates a fully configured plugin instance.
   *
   * @param string $scope
   *   Provides a scoped namespace for the plugin type being requested. This
   *   allows for providers to create identically named plugin types without
   *   needing to worry about panels and views caching plugins being delivered
   *   for core cache plugin requests.
   *
   * @param string $type
   *   The plugin type for the requested scope.
   *
   * @param array $options
   *   An optional array that will be passed to the factory class's
   *   getInstance() method.
   *
   * @param Context $context
   *   An optional context object.
   *
   * @return mixed
   *   A plugin instance whose type is dependent upon the $type parameter
   *   passed to this mapper object.
   */
  public function getPluginInstance($scope, $type, $options = array());

}
