<?php
/**
 * @file
 * Access Plugin Interface.
 */

namespace Drupal\Plugin\Access;

use Drupal\Plugin\PluginContextInterface;

/**
 * Access Plugin Interface for generic access handling.
 */
interface AccessInterface extends PluginContextInterface {

  /**
   * Determine the access to this plugin.
   *
   * @return bool
   */
  public function access();

}
