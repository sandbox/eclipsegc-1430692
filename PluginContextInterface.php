<?php
/**
 * @file
 * Interface implemented by plugins needing context support.
 */

namespace Drupal\Plugin;

use Symfony\Component\Routing\RequestContext;

/**
 * Plugin interface for handling context sensative plugins.
 */
interface PluginContextInterface extends PluginInterface {

  /**
   *  Get the current request context object.
   *
   *  @param $context
   *    A RequestContext object.
   */
  public function setContext(RequestContext $context);

  /**
   *  Limit the availability of this plugin by a certain type of context(s).
   *
   *  @return
   *    An array of context wrapper objects.
   *    TODO: No such thing as a context wrapper yet, should probably be a
   *    plugin type.
   */
  public function requiredContext();

}
