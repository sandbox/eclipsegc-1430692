<?php

namespace Drupal\Plugin\Responder;

use Drupal\Plugin\PluginInterface;
use Drupal\Core\DrupalKernel;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides an interface for all reponder plugin classes.
 */
interface ResponderInterface extends PluginInterface {

  /**
   *  Provides a generic response system designed to route to a specific 
   *  response. This method is generally used to define access (403), success
   *  (200), and various other potential response codes and corresponding
   *  output.
   *
   *  @param Request $request
   *    Symfony Request object.
   *
   *  @param DrupalKernel $kernel
   *    Drupal DrupalKernel object.
   *
   *  @return Symfony\Component\HttpFoundation\Response object
   */
  public function get(Request $request, DrupalKernel $kernel);
  public function post(Request $request, DrupalKernel $kernel);
  public function put(Request $request, DrupalKernel $kernel);
  public function delete(Request $request, DrupalKernel $kernel);

/**
   *  Provides a 404 error since these are not easily handling by the generic
   *  respond() method and need to be called separately.
   *
   *  @param Request $request
   *    Symfony Request object.
   *
   *  @param DrupalKernel $kernel
   *    Drupal DrupalKernel object.
   *
   *  @return Symfony\Component\HttpFoundation\Response object
   */
  public function send_404(Request $request, DrupalKernel $kernel);
}