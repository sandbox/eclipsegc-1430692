<?php
/**
 * @file
 * Context Plugin Interface.
 */

namespace Drupal\Plugin\Context;

use Drupal\Plugin\PluginContextInterface;

/**
 * Context Plugin Interface for checking contextual relevency.
 */
interface ContextWrapperInterface extends PluginContextInterface {

}
