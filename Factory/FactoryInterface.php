<?php
/**
 * @file
 * Factory interface implemented by all plugin factories.
 */

namespace Drupal\Plugin\Factory;

use Drupal\Config\DrupalConfig;

/**
 * Plugin factory interface.
 */
interface FactoryInterface {

  /**
   * Construct a plugin factory.
   *
   * The scope of a plugin type, and the type strings are passed here with the
   * intent of storing them on an on-going basis for the rest of the class to
   * utilize.
   *
   * @param string $scope
   *   The provider of the requested plugin type.
   * @param string $type
   *   The requested plugin type.
   * @param Configuration $type_configuration
   *   A configuration object containing information about the plugin type.
   */
  public function __construct($scope, $type, DrupalConfig $plugin_type);

  /**
   * Responsible for returning a preconfigured instance of a plugin.
   *
   * @param array $options
   *   Array of options by which the getConfiguration() method should be
   *   capable of determining which instance configuration to load and
   *   returning a config object with the plugin_class parameter populated.
   *
   * @return object
   *   A fully configured plugin instance.
   */
  public function getInstance($options);

}
