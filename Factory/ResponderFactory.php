<?php
/**
 * @file
 * Plugin factory for handling requests.
 */

namespace Drupal\Plugin\Factory;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Config\DrupalConfig;
use Drupal\Plugin\PluginException;

/**
 * Default Drupal Request factory.
 *
 * Provides logic for determining what plugin type handles a given request.
 */
class ResponderFactory implements FactoryInterface {

  protected $scope;
  protected $type;
  protected $plugin_type;

  /**
   * Implements FactoryInterface::__construct().
   */
  public function __construct($scope, $type, DrupalConfig $plugin_type) {
    $this->scope = $scope;
    $this->type = $type;
    $this->plugin_type = $plugin_type;
  }

  /**
   *  Responsible for loading the configuration of a plugin instance by the
   *  provided options array.
   *
   *  @param string $definition
   *    A string that corresponds to a given request plugin definition.
   *
   *  @return DrupalConfig object
   *    A relevant configuration object.
   */
  public function getConfiguration(Request $request) {
    $format = $request->headers->get('Content-Type');
    $format = $request->getFormat($format) ? $request->getFormat($format) : 'html';
    return config("plugin.{$this->scope}.{$this->type}.$format");
  }

  /**
   *  Responsible for finding the class relevant for a given plugin.
   *
   *  @param DrupalConfig $config
   *    A DrupalConfig object.
   *
   *  @return string
   *    The appropriate class name.
   */
  public function getPluginClass(DrupalConfig $definition) {
    $class_param = $this->plugin_type->get('class_param');
    if (empty($class_param)) {
      throw new PluginException("The plugin type class parameter is not specified.");
    }
    $class = $definition->get($class_param);
    if (empty($class)) {
      throw new PluginException("Plugin class was not specified.");
    }
    if (!class_exists($class)) {
      throw new PluginException(t("Plugin class @class does not exist.", array('@class' => $class)));
    }
    return $class;
  }

  /**
   * Implements FactoryInterface::getInstance().
   */
  public function getInstance($options) {
    $definition = $this->getConfiguration($options['request']);
    $plugin_class = $this->getPluginClass($definition);
    $instance = new $plugin_class($definition);
    return $instance;
  }

}
