<?php
/**
 * @file
 * Plugin factory for interacting with plugins that can consume context.
 */

namespace Drupal\Plugin\Factory;
use Symfony\Component\Routing\RequestContext;
use Drupal\Config\DrupalConfig;
use Drupal\Plugin\PluginException;
use Drupal\Plugin\PluginChildrenInterface;
use Drupal\Plugin\PluginContextInterface;

/**
 * Default Drupal Multiconfig factory.
 *
 * Provides logic for any basic plugin type that needs to provide individual
 * plugins based upon the current available contexts.
 */
class ContextFactory implements FactoryInterface {

  protected $scope;
  protected $type;
  protected $plugin_type;

  /**
   * Implements FactoryInterface::__construct().
   */
  public function __construct($scope, $type, DrupalConfig $plugin_type) {
    $this->scope = $scope;
    $this->type = $type;
    $this->plugin_type = $plugin_type;
  }

  /**
   *  Responsible for loading the configuration of a plugin instance by the
   *  provided options array.
   *
   *  @param array $options
   *    Array of options by which the getConfiguration() method should be
   *    capable of determining which instance configuration to load and
   *    returning a config object with the plugin_class parameter populated.
   *
   *  @return object
   *    A relevant configuration object.
   */
  public function getConfiguration($options) {
    return config($options['config']);
  }

  /**
   *  Responsible for finding the class relevant for a given plugin.
   *
   *  @param DrupalConfig $config
   *    A DrupalConfig object.
   *
   *  @return string
   *    The appropriate class name.
   */
  public function getPluginClass(DrupalConfig $config) {
    $class_param = $this->plugin_type->get('class_param');
    if (empty($class_param)) {
      throw new PluginException("The plugin type class parameter is not specified.");
    }
    $class = $config->get($class_param);
    if (empty($class)) {
      throw new PluginException("Plugin class was not specified.");
    }
    if (!class_exists($class)) {
      throw new PluginException(t("Plugin class @class does not exist.", array('@class' => $class)));
    }
    return $class;
  }

  /**
   * Implements FactoryInterface::getInstance().
   */
  public function getInstance($options) {
    $config = $this->getConfiguration($options);
    $plugin_class = $this->getPluginClass($config);
    $instance = new $plugin_class($config);
    if ($instance instanceof PluginChildrenInterface && isset($options['child'])) {
      $instance->getChild($options['child']);
    }
    if ($instance instanceof PluginContextInterface) {
      $container = drupal_container();
      $instance->setContext($container['context']);
    }
    return $instance;
  }

}
